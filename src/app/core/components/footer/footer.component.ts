import { Component, OnInit } from '@angular/core';
import { RequestRestaurantService } from 'src/app/shared/services/request-restaurant.service';
import { Ifooter, Iimage } from '../../models/ifooter';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {
  public footerInfo: any;
  public socialMedia?: Iimage[];
  public logo?: Iimage;
  constructor(private RequestRestaurantService: RequestRestaurantService) { 
    this.logo={
      src: '../../../assets/images/logo.png',
      alt: 'Logo Pizzeria',
      path: ''
    },
    this.socialMedia=[
      {
        src: '../../../assets/images/svg/facebook.svg',
        alt: 'facebook'
      },
      {
        src: '../../../assets/images/svg/twitter.svg',
        alt: 'twitter'
      },
      {
        src: '../../../assets/images/svg/instagram.svg',
        alt: 'instagram'
      }
    ]
  }

  ngOnInit(): void {
    this.getFooter()
  }
  public getFooter(){
    this.RequestRestaurantService.getInfoFooter().subscribe(data =>{
      this.footerInfo = data;
      console.log(this.footerInfo[2].footerContent.copyright);
    })
  }
}
