import { Component, OnInit } from '@angular/core';
import { Iheader, Iimage } from '../../models/iheader';
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  public navBarImage: Iimage = {
    src: '../../../assets/images/logo.png',
    alt: 'Logo Pizzeria',
    path: ''
  }
  public navBar: Iheader[]=[
    {
      name: 'Home',
      path: 'home'
    },
    {
      name: 'About',
      path: 'about'
    },
    {
      name: 'Blog',
      path: 'blog'
    },
  ]


  constructor() { }

  ngOnInit(): void {
  }

}
