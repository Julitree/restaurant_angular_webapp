export interface Ifooter {
    logo: Iimage,
    email: string,
    phone: string,
    copyright: string
}

export interface Iimage{
    src:string,
    alt:string,
    path?: string
}