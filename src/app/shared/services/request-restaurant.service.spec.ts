import { TestBed } from '@angular/core/testing';

import { RequestRestaurantService } from './request-restaurant.service';

describe('RequestRestaurantService', () => {
  let service: RequestRestaurantService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(RequestRestaurantService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
