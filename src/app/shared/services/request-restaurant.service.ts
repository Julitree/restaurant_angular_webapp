import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { Ihome } from 'src/app/features/home-page/home-models/ihome';



@Injectable()
export class RequestRestaurantService {
  private endpoint = 'http://localhost:3000';
  constructor(private HttpClient: HttpClient) { }

  getInfoHome():Observable <Ihome[]>{
    return this.HttpClient.get(`${this.endpoint}/home`).pipe(
      map((res: any)=>{
      if(!res){
        throw new Error('Value expected!');
      }else{
        const results: any[] =  res;
        const formattedResults = results.map(({ heroContent, aboutHome, pizzaTypes,ourPizzas}) => ({
          heroContent,
          aboutHome,
          pizzaTypes,
          ourPizzas
        }));
        return formattedResults;
      }
    }),
    catchError(err =>{
      throw new Error(err.message)
    }))
  } 
  getInfoFooter(){
    return this.HttpClient.get(`${this.endpoint}/footer`).pipe(
      map((res: any)=>{
      if(!res){
        throw new Error('Value expected!');
      }else{
        const results: any[] =  res;
        const formattedResults = results.map(({ imagesGallery, decorationLineSeeAllMenu,footerContent}) => ({
          imagesGallery,
          decorationLineSeeAllMenu,
          footerContent
        }));
        return formattedResults;
      }
    }),
    catchError(err =>{
      throw new Error(err.message)
    }))
  }

  getInfoAbout(){
    return this.HttpClient.get(`${this.endpoint}/about`).pipe(
      map((res: any)=>{
      if(!res){
        throw new Error('Value expected!');
      }else{
        const results: any[] =  res;
        const formattedResults = results.map(({ hero}) => ({
          hero
        }));
        return formattedResults;
      }
    }),
    catchError(err =>{
      throw new Error(err.message)
    }))
  }
  
}

