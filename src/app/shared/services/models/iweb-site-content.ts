export interface IwebSiteContent {
      posts: Iinfo[];
      comments: Iinfo[];
      profile: string
  }
export interface Iinfo{
  id: number,
  title: string,
  author: string
}

export interface profile{
  name: string
}