import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ButtonComponent } from './components/button/button.component';
import { ImagesGalleryFooterComponent } from './components/images-gallery-footer/images-gallery-footer.component';
import { DecorationLineSeeAllMenuComponent } from './components/decoration-line-see-all-menu/decoration-line-see-all-menu.component';



@NgModule({
  declarations: [
    ImagesGalleryFooterComponent,
    DecorationLineSeeAllMenuComponent, 
    ButtonComponent
  ],
  imports: [
    CommonModule
  ],
  exports:[
    ImagesGalleryFooterComponent,
    DecorationLineSeeAllMenuComponent, 
    ButtonComponent
  ]
})
export class SharedModule { }
