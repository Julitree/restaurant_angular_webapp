import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ImagesGalleryFooterComponent } from './images-gallery-footer.component';

describe('ImagesGalleryFooterComponent', () => {
  let component: ImagesGalleryFooterComponent;
  let fixture: ComponentFixture<ImagesGalleryFooterComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ImagesGalleryFooterComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ImagesGalleryFooterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
