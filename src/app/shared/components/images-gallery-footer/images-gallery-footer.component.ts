import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-images-gallery-footer',
  templateUrl: './images-gallery-footer.component.html',
  styleUrls: ['./images-gallery-footer.component.scss']
})
export class ImagesGalleryFooterComponent implements OnInit {
@Input() imageRequestFooter: any;
  constructor() { }

  ngOnInit(): void {
  }

}
