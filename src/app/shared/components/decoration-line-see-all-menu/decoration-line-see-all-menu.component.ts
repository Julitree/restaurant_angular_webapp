import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-decoration-line-see-all-menu',
  templateUrl: './decoration-line-see-all-menu.component.html',
  styleUrls: ['./decoration-line-see-all-menu.component.scss']
})
export class DecorationLineSeeAllMenuComponent implements OnInit {
@Input() getTextLineSeeAllMenu: any;
  constructor() { }

  ngOnInit(): void {
  }

}
