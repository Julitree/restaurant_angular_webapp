import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DecorationLineSeeAllMenuComponent } from './decoration-line-see-all-menu.component';

describe('DecorationLineSeeAllMenuComponent', () => {
  let component: DecorationLineSeeAllMenuComponent;
  let fixture: ComponentFixture<DecorationLineSeeAllMenuComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DecorationLineSeeAllMenuComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DecorationLineSeeAllMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
