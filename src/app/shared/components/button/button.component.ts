import { Component, Input, OnInit } from '@angular/core';
import { Ibutton } from '../../models/iButton';

@Component({
  selector: 'app-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.scss']
})
export class ButtonComponent implements OnInit {
@Input() button?: Ibutton;
  constructor() { 
    this.button = {
      info: "all menu",
      path: ""
    }
      
  }

  ngOnInit(): void {
  }

}
