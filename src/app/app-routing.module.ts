import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'home',
    loadChildren: ()=> import('./features/home-page/home-module').then((module)=>module.HomeModule)
  },
  {
    path: 'about',
    loadChildren: ()=> import('./features/about-page/about-module').then((module)=>module.AboutModule)
  },
  {
    path: 'blog',
    loadChildren: ()=> import('./features/blog-page/blog-module').then((module)=>module.BlogModule)
  }
];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
