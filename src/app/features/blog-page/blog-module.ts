import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeRoutingModule } from './blog-routing.module';

import { RequestRestaurantService } from 'src/app/shared/services/request-restaurant.service';
import { HttpClientModule } from '@angular/common/http';
import { BlogComponent } from './blog-comp/blog.component';


@NgModule({
  declarations: [BlogComponent],
  imports: [
    CommonModule,
    HomeRoutingModule,
    HttpClientModule
  ],
  providers: [
    RequestRestaurantService
  ]
})
export class BlogModule { }

