import { Component, OnInit } from '@angular/core';
import { RequestRestaurantService } from 'src/app/shared/services/request-restaurant.service';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss']
})
export class AboutComponent implements OnInit {

  public myAbout: any;
  public myFooter: any;

  constructor(private RequestRestaurantService: RequestRestaurantService) { }

  ngOnInit(): void {
    this.getAbout();
    this.getFooter();
  }

  public getAbout(){
    this.RequestRestaurantService.getInfoAbout().subscribe(data =>{
      this.myAbout = data;
      console.log(this.myAbout[0]);

    })
  }

  public getFooter(){
    this.RequestRestaurantService.getInfoFooter().subscribe(data =>{
      this.myFooter = data;
      console.log(this.myFooter[1].decorationLineSeeAllMenu);
    })
  }
}
