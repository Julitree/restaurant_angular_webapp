import { Component, Input, OnInit } from '@angular/core';
import { IheroAbout } from '../../about-models/iabout';


@Component({
  selector: 'app-hero-about',
  templateUrl: './hero-about.component.html',
  styleUrls: ['./hero-about.component.scss']
})
export class HeroAboutComponent implements OnInit {
@Input() heroABout!: IheroAbout
  constructor() { }

  ngOnInit(): void {
  }

}
