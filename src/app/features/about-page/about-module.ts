import { HttpClientModule } from '@angular/common/http';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { AboutComponent } from './about-comp/about.component';
import { HomeRoutingModule } from './about-routing.module';
import { RequestRestaurantService } from 'src/app/shared/services/request-restaurant.service';
import { HeroAboutComponent } from './about-comp/hero-about/hero-about.component';
import { SharedModule } from 'src/app/shared/shared.module';



@NgModule({ 
  declarations: [AboutComponent, HeroAboutComponent],
  imports: [
    CommonModule,
    HomeRoutingModule,
    HttpClientModule,
    SharedModule

  ],
  providers: [
    RequestRestaurantService
  ]
})
export class AboutModule { }

