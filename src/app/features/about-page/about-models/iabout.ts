export interface Iabout {
    heroContent? : IheroAbout;
    aboutHome?: Iabout;
}

export interface IheroAbout {
    id: string;
    title: string;
    subtitle: string;
    src: string 
}

export interface Iabout {
    title: string;
    content: string;
}
