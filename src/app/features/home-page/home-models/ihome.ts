export interface Ihome {
    heroContent? : IheroContent;
    aboutHome?: IaboutHome;
    pizzaTypes? : IpizzaTypes;
    ourPizzas? : IourPizzas;
    footer?: Ifooter
}

export interface IheroContent {
    id: string;
    title: string;
    subtitle: string;
    src: string 
}
 
export interface IaboutHome {
    title: string;
    content: string;
}

export interface IpizzaTypes{
    pizzaTypes: string[]
}

export interface IourPizzas{
    titleSection: IaboutHome;
    items: IpizzaTypes
}

export interface Ifooter{
    imagesGallery: string[]
}
