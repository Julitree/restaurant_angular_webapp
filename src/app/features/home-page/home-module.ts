// import { HomeService } from '../../shared/services/home.service';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { CommonModule } from '@angular/common';
import { HomeRoutingModule } from './home-routing.module';

import { HomeComponent } from './home-comp/home.component';
import { RequestRestaurantService } from 'src/app/shared/services/request-restaurant.service';
import { HeroComponent } from './home-comp/hero/hero.component';
import { HomeAboutComponent } from './home-comp/home-about/home-about.component';
import { TypesPizzaBannerComponent } from './home-comp/types-pizza-banner/types-pizza-banner.component';
import { GridPizzasComponent } from './home-comp/grid-pizzas/grid-pizzas.component';
import { SharedModule } from 'src/app/shared/shared.module';

 


@NgModule({
  declarations: [
    HomeComponent, 
    HeroComponent, 
    HomeAboutComponent,
    TypesPizzaBannerComponent,
    GridPizzasComponent,

],
  imports: [
    CommonModule,
    HomeRoutingModule,
    HttpClientModule,
    SharedModule
  ],
  providers: [
    RequestRestaurantService
  ]
})
export class HomeModule { }

