import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GridPizzasComponent } from './grid-pizzas.component';

describe('GridPizzasComponent', () => {
  let component: GridPizzasComponent;
  let fixture: ComponentFixture<GridPizzasComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GridPizzasComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GridPizzasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
