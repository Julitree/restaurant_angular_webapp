import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-grid-pizzas',
  templateUrl: './grid-pizzas.component.html',
  styleUrls: ['./grid-pizzas.component.scss']
})
export class GridPizzasComponent implements OnInit {
@Input() gridPizzasInfo: any;
  constructor() { }

  ngOnInit(): void {
  }

}
