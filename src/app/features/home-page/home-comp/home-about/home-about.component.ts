import { Component, Input, OnInit } from '@angular/core';
import { IaboutHome } from '../../home-models/ihome';

@Component({
  selector: 'app-home-about',
  templateUrl: './home-about.component.html',
  styleUrls: ['./home-about.component.scss']
})
export class HomeAboutComponent implements OnInit {
@Input() aboutHome!: IaboutHome;
  constructor() { }

  ngOnInit(): void {
  }

}
