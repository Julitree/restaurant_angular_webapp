import { Component, Input, OnInit } from '@angular/core';
import { IheroContent } from '../../home-models/ihome';

@Component({
  selector: 'app-hero',
  templateUrl: './hero.component.html',
  styleUrls: ['./hero.component.scss']
})
export class HeroComponent implements OnInit {
  @Input() hero!: IheroContent;
  constructor() { }

  ngOnInit(): void {
  } 
 
}
