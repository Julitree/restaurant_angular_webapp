import { Component, OnInit } from '@angular/core';
import { RequestRestaurantService } from 'src/app/shared/services/request-restaurant.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  public myHome: any;
  public myFooter: any;
  constructor(private RequestRestaurantService: RequestRestaurantService) { }

  ngOnInit(): void {
    this.getHome()
    this.getFooter()

  }

  public getHome(){
    this.RequestRestaurantService.getInfoHome().subscribe(data =>{
      this.myHome = data;
    })
  }

  public getFooter(){
    this.RequestRestaurantService.getInfoFooter().subscribe(data =>{
      this.myFooter = data;
      console.log(this.myFooter[1].decorationLineSeeAllMenu);
    })
  }

}
