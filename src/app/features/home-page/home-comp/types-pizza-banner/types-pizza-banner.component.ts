import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-types-pizza-banner',
  templateUrl: './types-pizza-banner.component.html',
  styleUrls: ['./types-pizza-banner.component.scss']
})
export class TypesPizzaBannerComponent implements OnInit {
@Input() typesPizzaBannerInfo: any;
  constructor() { }

  ngOnInit(): void {
  }

}
