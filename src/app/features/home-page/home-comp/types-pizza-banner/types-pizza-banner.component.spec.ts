import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TypesPizzaBannerComponent } from './types-pizza-banner.component';

describe('TypesPizzaBannerComponent', () => {
  let component: TypesPizzaBannerComponent;
  let fixture: ComponentFixture<TypesPizzaBannerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TypesPizzaBannerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TypesPizzaBannerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
